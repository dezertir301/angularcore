<?php

namespace app\components\db;
/**
* This is the ActiveQuery class
 *
 * @see ActiveQuery
*/
class ActiveQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     */
    public function active()
    {
        $this->andWhere('[[active]]=1');
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function all($db = null)
    {
        $this->active();
        return parent::all($db);
    }

    /**
     * @inheritdoc
     */
    public function one($db = null)
    {
        $this->active();
        return parent::one($db);
    }
}