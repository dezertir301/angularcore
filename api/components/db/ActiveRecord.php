<?php
/**
 * Created by PhpStorm.
 * User: dez
 * Date: 11.09.15
 * Time: 16:13
 */

namespace app\components\db;

/**
 * Class ActiveRecord
 * @package app\components\db
 * Base class for ActiveRecord
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    const ACTIVE = 1;
    const INACTIVE = 0;
    const ACTIVE_FIELD = "active";


    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new \app\components\db\ActiveQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function delete()
    {
        if ($this->hasAttribute(static::ACTIVE_FIELD)) {
            $this->active = $this::INACTIVE;

            if ($this->save(null, [static::ACTIVE_FIELD])) {
                return 1;
            }
        } else {
            return parent::delete();
        }
    }

}