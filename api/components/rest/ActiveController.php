<?php
/**
 * Created by PhpStorm.
 * User: dez
 * Date: 11.09.15
 * Time: 16:07
 */

namespace app\components\rest;


use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * Class ActiveController
 * @package app\components\rest
 * Base class for all ActiveControllers
 */
class ActiveController extends  \yii\rest\ActiveController
{
    public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            /*'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function () {
                    throw new ForbiddenHttpException("Access dined");
                }
            ],*/
        ];
    }

    public function actions()
    {
        $actions = parent::actions();

        //Remove pagination headers
        $actions["index"]["prepareDataProvider"] = function () {
            $class = $this->modelClass;
            return new ActiveDataProvider([
                "query" => $class::find(),
                "pagination" => false
            ]);
        };

        return $actions;
    }

}