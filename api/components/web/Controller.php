<?php

namespace app\components\web;

use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;

/**
 * Class Controller
 * @package app\components\web
 * Base class for all common Controllers
 */
class Controller extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function () {
                    throw new UnauthorizedHttpException("Not authorized user");
                }
            ],
        ];
    }
}