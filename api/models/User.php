<?php

namespace app\models;

use app\components\db\ActiveRecord;
use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "User".
 *
 * @property integer $userId
 * @property string $firstName
 * @property string $lastName
 * @property string $password
 * @property integer $ban
 * @property string $role
 * @property integer $active
 */
class User extends ActiveRecord implements IdentityInterface
{
    const ROLE_ADMIN = "admin";
    const ROLE_USER = "user";

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'User';
    }

    public function fields() {
        $fields = parent::fields();

        unset($fields["password"]);

        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password','email', 'role'], 'required'],
            [['ban', 'active'], 'integer'],
            [['role'], "in", "range" => [static::ROLE_ADMIN, static::ROLE_USER]],
            [['firstName', 'lastName'], 'string', 'max' => 255],
            [['password'], 'string', 'max' => 64],
            ['email', 'unique', 'targetAttribute' => 'email'],
            ['password', 'filter', 'filter' => function ($password) {
                if($this->isNewRecord || $this->isAttributeChanged("password")) {
                    return Yii::$app->getSecurity()->generatePasswordHash($password, 4);
                } else {
                    return $password;
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userId' => 'User ID',
            'firstName' => 'First Name',
            'lastName' => 'Last Name',
            'password' => 'Password',
            'ban' => 'Ban',
            'role' => 'Role',
            'active' => 'Active',
        ];
    }

    public static function findUserByEmailAndPass($email, $password) {

        $user = static::find()->where([
            'email' => $email,
        ])->one();

        if ($user && Yii::$app->getSecurity()->validatePassword($password, $user->password)) {
            return $user;
        }

        return null;
    }

    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->primaryKey;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }
}
