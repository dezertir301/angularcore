<?php

use yii\db\Schema;
use yii\db\Migration;

class m151116_075542_init extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `User` (
          `userId` INT NOT NULL AUTO_INCREMENT,
          `email`  VARCHAR(255) NOT NULL,
          `firstName` VARCHAR(255) NULL,
          `lastName` VARCHAR(255) NULL,
          `password` VARCHAR(64) NOT NULL,
          `ban` TINYINT(1) NOT NULL DEFAULT 0,
          `role` ENUM('admin','user') NOT NULL,
          `active` TINYINT(1) NOT NULL DEFAULT 1,
          PRIMARY KEY (`userId`))
        ENGINE = InnoDB");

        $this->insert("User", [
            "email" => "admin@iqria.com",
            //test123
            "password" => '$2y$13$V1KT/Gu98PkmVge5W6DdweEBCTleHD6WtUifaKdhyQ9iblBKPCqDG',
            "firstName" => "Super",
            "lastName" => "Admin",
            "role" => "admin"
        ]);
    }

    public function down()
    {
        echo "m151116_075542_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
