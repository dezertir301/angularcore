<?php

namespace app\controllers;

use app\components\mail\VoteMailer;
use app\models\Employee;
use app\models\User;
use Yii;
use app\components\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;

/**
 * Class AuthenticateController
 * @package app\controllers
 * Controller for login, logout and check authenticate
 */
class AuthController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'check' => ['get'],
                    'login' => ['post'],
                    'logout' => ['get'],
                ]
            ]
        ];
    }

    /**
     * @return null|\yii\web\IdentityInterface
     * @throws UnauthorizedHttpException
     * check identity in session
     */
    public function actionCheck()
    {
        if (Yii::$app->user->getId()) {
            return Yii::$app->user->identity;
        } else {
            throw new UnauthorizedHttpException("Not authorized user");
        }
    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     * log in system
     */
    public function actionLogin()
    {
        $post = Yii::$app->request->post();
        $login = $post["email"];
        $password = $post["password"];

        if ($identity = User::findUserByEmailAndPass($login, $password)) {
            Yii::$app->user->login($identity);
            return $identity;
        } else {
            throw new NotFoundHttpException("Email or password are not valid");
        }
    }

    /**
     * @throws UnauthorizedHttpException
     * Sign Out action
     */
    public function actionLogout()
    {
        if (Yii::$app->user->getId()) {
            Yii::$app->user->logout(true);
            Yii::$app->response->statusCode = 204;
        } else {
            throw new UnauthorizedHttpException("Not authorized user");
        }

    }
}
