<?php

namespace app\controllers;

use app\models\User;

class UserController extends \app\components\rest\ActiveController
{
    public $modelClass = User::class;
}
