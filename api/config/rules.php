<?php
    return [
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'auth',
            'patterns' => [
                'GET check' => 'check',
                'POST login' => 'login',
                'GET logout' => 'logout'
            ],
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'user'
        ],

    ];