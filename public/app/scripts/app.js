angular
  .module("CM", [
    /*"minovateApp",*/
    "Auth",
    "ui.router",
    "js-data",
    'toastr',
    "picardy.fontawesome",
    "ui.bootstrap",
    "ngTable",
    "material.components.input",
    "ngMessages"
  ])
  .run(['$rootScope', '$state', '$stateParams', "AuthService", function($rootScope, $state, $stateParams, authService) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
      var requireLogin = toState.data && toState.data.login;

      //check login before rout
      if (requireLogin) {

        authService.check()
          .catch(function () {
            event.preventDefault();
            $state.go("core.login");
          });
      }
    });


    $rootScope.$on('$stateChangeSuccess', function(event, toState) {
      event.targetScope.$watch('$viewContentLoaded', function () {
        angular.element('html, body, #content').animate({ scrollTop: 0 }, 200);

        setTimeout(function () {
          angular.element('#wrap').css('visibility','visible');

          if (!angular.element('.dropdown').hasClass('open'))
            angular.element('.dropdown').find('>ul').slideUp();
        }, 200);
      });
    });
  }]);