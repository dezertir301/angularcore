angular
  .module("CM")
  .config(["DSHttpAdapterProvider", function (DSHttpProvider) {

    angular.extend(DSHttpProvider.defaults, {
      //base Path to all queries JSDATA
      basePath: "/api"
    });
  }]);