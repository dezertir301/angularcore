angular
  .module("CM")
  .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/core/login');

    $stateProvider
      .state('core', {
        url: '/core',
        abstract: true,
        templateUrl: "views/home.html",
      })
      .state('core.login', {
        url: '/login',
        templateUrl: 'views/auth/login.html',
        controller: "AuthController",
      })

      .state("app", {
        url: '/app',
        abstract: true,
        templateUrl: "views/layout/layout.html",
        data: {
          login: true
        }
      })
      .state("app.users", {
        url: "/users",
        templateUrl: "views/user/index.html",
        data: {
          login: true
        },
        controller: "UserController",
      })
  }]);