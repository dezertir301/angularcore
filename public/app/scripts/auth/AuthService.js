angular
  .module("Auth")
  .factory("AuthService", ["$http", "$q", "User", function ($http, $q, User) {
    return {
      baseUrl: "/api/auth/",

      user: null,

      check: function () {
        if (this.user) {
          return $q.when(this.user);
        }

        return $http.get(this.baseUrl + "check")
          .then(function (response) {
            this.user = User.createInstance(response.data);

            return this.user;
          }.bind(this));
      },

      login: function (email, password) {
        return $http.post(this.baseUrl + "login", {
            "email": email,
            "password": password
          })
          .then(function (response) {
            this.user = User.createInstance(response.data);

            return this.user;
          }.bind(this));
      },

      logout: function () {
        return $http.get(this.baseUrl + "logout");
      },

      getUser: function () {
        return this.user;
      }

    };
  }]);