angular
  .module("Auth")
  .controller("AuthController", ["$scope", "$state", "AuthService", "toastr", function ($scope, $state, authService, toast) {
    $scope.user = {};

    $scope.login = function () {
      authService
        .login(this.user.email, this.user.password)
        .then(function (user) {
          $state.go(user.getDefaultState());
        })
        .catch(function (errors) {
          toast.error(errors.data.message);
        });
    };

  }]);
