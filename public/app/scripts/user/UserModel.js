angular
  .module("CM")
  .factory("User",  ["DS", function (DS) {
    var BAN = 1,
        UNBAN = 0;

    return DS.defineResource({

      name: "User",

      idAttribute: "userId",

      endpoint: "/users",

      computed: {
        fullName: ['firstName', 'lastName', function (first, last) {
          return first + " " + last;
        }]
      },

      BAN: 1,

      methods: {
        getDefaultState: function () {
          var defaultState = "";

          switch (this.role) {
            case "admin":
              defaultState = "app.users";
              break;
            case "user":
              defaultState = "app.companies";
              break;
          }

          return defaultState;

        },
        block: function () {
          return this.DSUpdate({ban: BAN});
        },
        unlock: function () {
          return this.DSUpdate({ban: UNBAN});
        }
      }

    });
  }]);