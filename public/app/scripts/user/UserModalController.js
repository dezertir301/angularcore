angular.module("CM")
  .controller("UserModalController", [
    "$scope",
    "$modalInstance",
    "userInstance",
    "User",
    function ($scope, $modalInstance, user, User) {
      $scope.user = user;

      $scope.submit = function () {
        if (!$scope.form.$valid) {
          debugger;
          return $scope.form.$error.required.forEach(function (control) {
            control.$setTouched();
          });
        }

        if (user.userId) {
          user.DSSave();
        } else {
          User.create(user);
        }

        $modalInstance.close(user);
      };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      }

    }]);