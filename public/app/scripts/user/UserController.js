angular.module("CM")
  .controller("UserController", [
    "$scope",
    "$modal",
    "User",
    "ngTableParams",
    "UserModalService",
    function ($scope, $modal, userModel, ngTableParams, UserModalService) {
      $scope.tableParams = new ngTableParams(
        {
          page: 1,
          count: 1,
          sorting: {
            name: 'asc'
          }
        },
        {
          counts: [],
          getData: function (defer) {
            return userModel.findAll().then(function (users) {
              return defer.resolve(users);
            });
          }
        });

      $scope.block = function (user, index) {
        user.block().then(function (user) {
          $scope.tableParams.data[index] = user;
        });
      };
      $scope.unlock = function (user, index) {
        user.unlock().then(function (user) {
          $scope.tableParams.data[index] = user;
        });
      };

      $scope.edit = function (editedUser, index) {
        UserModalService.open(editedUser)
          .then(function (user) {
            console.log(user);
            if (index !== undefined) {
              $scope.tableParams.data[index] = user;
            } else {
              $scope.tableParams.data.push(user);
            }
          })
          .catch(function(isPhantom) {
            if (!isPhantom)
              $scope.tableParams.data.splice(index, 1, initial);
          });
      };

      $scope.addNew = function () {
        $scope.edit();
      }
  }]);