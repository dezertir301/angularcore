angular.module("CM")
  .factory("UserModalService", ["$modal", function ($modal) {

    return {
      open: function (user) {
        return $modal.open({
          templateUrl: 'views/user/modal.html',
          controller: 'UserModalController',
          backdropClass: 'splash splash-2',
          windowClass: 'splash splash-2  splash-ef-10',
          backdrop : 'static',
          resolve: {
            userInstance: function () {
              return user || {};
            }
          }
        }).result;
      }
    }

  }]);