angular.module("CM")
  .controller("MainController", ["$scope", function ($scope) {
    this.title = 'Minovate';

    this.settings = {
      navbarHeaderColor: 'scheme-default',
      sidebarColor: 'scheme-default',
      brandingColor: 'scheme-default',
      activeColor: 'default-scheme-color',
      headerFixed: true,
      asideFixed: true,
      rightbarShow: false
    };

}]);