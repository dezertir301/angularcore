module.exports = function(grunt) {
  // Задачи
  grunt.initConfig({
    // Склеиваем
    concat: {
      main: {
        src: [
          'app/**/*.js',
          'app.js'
        ],
        dest: 'dist/scripts/scripts.js'
      },
      theme: {
        src: [
          'vendor/minovate/app/scripts/**.js'
        ],
        dest: 'dist/scripts/theme.js'
      }
    },
    //Подключаем все файлы которые скачал bower
    bower_concat: {
      all: {
        dest: 'dist/scripts/vendor.js', // Склеенный файл
      },
    },
    compass: {                  // Task
      dist: {                   // Target
        options: {              // Target options
          sassDir: "app/styles",
          cssDir: "dist/styles",
          generatedImagesDir: '.tmp/images/generated',
          imagesDir: 'dist/images',
          javascriptsDir: 'dist/scripts',
          fontsDir: 'dist/styles/fonts',
          importPath: './bower_components',
          httpImagesPath: '/images',
          httpGeneratedImagesPath: '/images/generated',
          httpFontsPath: '/styles/fonts',
          relativeAssets: false,
          assetCacheBuster: false,
        },
        files: 'app/styles/**/*.scss'
      }
    },
    copy: {
      main: {
        files: [
          {
            dot: true,
            expand: true,
            cwd: 'app/',
            src: '*.*',
            dest: 'dist/'
          },
          {
            expand: true,
            cwd: 'app/images',
            src: '*.*',
            dest: 'dist/images'
          },
          {
            src: 'bower_components/bootstrap-sass-official/assets/fonts/bootstrap/*',
            dest: 'dist/'
          }
        ]
      },
      views: {
        expand: true,
        cwd: 'app/views',
        src: '*.*',
        dest: 'dist/views'
      },
      theme: {
        files: [
          {
            expand: true,
            cwd: 'vendor/minovate/dist/',
            src: ["*/**"],
            dest: "dist/",
          }
        ]

      }
    },

    // Сжимаем
    uglify: {
      options: {
        separator: ';'
      },
      main: {
        files: {
          // Результат задачи concat
          'dist/scripts/scripts.min.js': '<%= concat.main.dest %>'
        }
      },
      bower: {
        files: {
          'dist/scripts/vendor.min.js': '<%= bower_concat.all.dest %>'
        }
      }
    },
    //Склеивание в реальном времени
    watch: {
      scripts: {
        files: ['<%= concat.main.src %>',"<%= compass.dist.files %>","<%= copy.views.cwd %>/*"],
        tasks: ['concat',"compass", "copy:views"],
        options: {
          spawn: false,
          livereload: true,
        }
      }
    },
    run: {
      minovate: {
        exec: "npm install && bower install && grunt build",
        options: {
          cwd: "vendor/minovate/"
        }
      }
    }
  });

  // Загрузка плагинов, установленных с помощью npm install
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-bower-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-run-grunt');
  grunt.loadNpmTasks('grunt-run');
  // Задача по умолчанию
  grunt.registerTask('build', [/*'run'*/'concat','copy','bower_concat','compass',/*'uglify'*/]);
  grunt.registerTask('default', ['concat','copy:main','copy:views','compass',/*'uglify'*/]);
};